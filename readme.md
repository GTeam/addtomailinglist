# Add To Mailing List

AddToMailinglist is a add-on (MailExtension) for Thunderbird.

It add the current email address to an existing mailing list.

This add-on adds a button to the window for composing/reading an email. You can choose a mailing list to add this email address to. 

## Code Source
[UrlToJD2](https://framagit.org/GTeam/addtomailinglist)

## Installation
[officiel site](https://support.mozilla.org/en-US/kb/installing-addon-thunderbird)

## License
This project is licensed under the Mozilla Public License, version 2.0

