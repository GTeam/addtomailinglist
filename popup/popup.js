function authorToEmail(text){
  const regexMail = /<(.+)>/i;
  let matches = text.match(regexMail);
    if (matches == null){ // no name
      return text;
    } else { // email
      return matches[1];
    }
}

function notify(email, listName) {
  var message = browser.i18n.getMessage("extensionNotificationInMessage") + listName;
  browser.notifications.create({
    "type": "basic",
    "iconUrl": browser.runtime.getURL("icons/addlist2.png"),
    "title": email,
    "message": message
  });
}

// click event
function addToMailingList(listId, listName, email) {
  // search contact
  // console.log(listId + ' fonction addToMailingList');
  browser.contacts.quickSearch(email)
  .then((arrayContacts) => {
    var waitForId;
    if (arrayContacts.length == 0){
      // create ContactNode
      waitForId = browser.mailingLists.get(listId)
      .then((node) => {
        console.log('create contact');
        return browser.contacts.create(node.parentId, {"DisplayName" : email, "PrimaryEmail" : email})
      }).then((id) => {
        // console.log('new: ' + id);
        return id
      })
    } else {
      // on prend le premier (on s'en fout...)
      waitForId = Promise.resolve(arrayContacts[0].id);
      // console.log('exist waitForId: ' + waitForId);  
    }
    return waitForId;
  })
  // add contact to mailingList
  .then((contactId) => {
    // console.log('add: ' + contactId + ' -- listId: ' + listId + ' -- listName: ' + listName);
    browser.mailingLists.addMember(listId, contactId);
    notify(email, listName); 
    // window.close(); // bug OK in 83 ?
  })

}


var txtMakeChoice = browser.i18n.getMessage("extensionMessageDisplayActionChoice");
document.getElementById('choice').innerHTML = txtMakeChoice;

var email;

browser.tabs.query({
  active: true,
  currentWindow: true,
}).then(tabs => {
  let tabId = tabs[0].id;
  return browser.messageDisplay.getDisplayedMessage(tabId)
}).then((message) => {
  // console.log('the email (message)');
  // console.log(message);
  email = authorToEmail(message.author);
  // console.log(email);
}).then(() => {
  return browser.addressBooks.list(true)
}).then((listBooks) => {
  for (let aBook of listBooks){
    for (let aList of aBook.mailingLists){
      let li = document.createElement("li");
      li.textContent = aList.name;
      li.addEventListener("click", () => addToMailingList(aList.id, aList.name, email));
      document.getElementById("mailingLists").appendChild(li);
    }
  }
});
